const express = require('express');
var cors = require('cors');
const db = require('./db');

const app = express();
const port = 3000;

app.use(
    cors({origin: '*'})
);

let date = new Date;
date.setDate(date.getDate() - 30);

app.get('/', async (req, res) => {
    try {
        const results = await db.query(`SELECT ST_Y(location), ST_X(location), 250-abs(rssi) as "rssi", gateway_id from mtmainservice.etsignalstrength where ST_Y(location) > 0 and time_index > '${date.toISOString().replace('T', ' ').replace('Z', '')}';`);
        let values = [];
        for (var row in results.rows){
            values.push([[results.rows[row].st_y, results.rows[row].st_x, results.rows[row].rssi], results.rows[row].gateway_id]);
        }
        res.json(values);
    } catch (err) {
        console.error(err);
        res.status(500).send('Internal Server Error');
    }
});

app.listen(port, () => {
    console.log('Server is running on port 3000');
})