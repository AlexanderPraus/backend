const { Pool} = require('pg');

console.log(process.env);

const db = new Pool({
    host: process.env.PG_HOST,
    user: process.env.PG_USER,
    password: process.env.PG_PASSWORD,
    database: process.env.PG_DB,
    port: 5432
});

console.log(db);

module.exports = {
    query: (text, params) => db.query(text, params)
  };